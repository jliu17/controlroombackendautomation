package com.AA.credentialVaultTests.functional;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import com.AA.CommonUtil.BaseTest;
import com.AA.credentialVaultAPI.CredentialVaultAPI;

public class CredentialsNegativeTest extends BaseTest {
	String uri = "/v1/credentialvault/";
	String name = "automationtest";
	CredentialVaultAPI cvAPI = new CredentialVaultAPI();
	
	@Test(priority = 1)
	public void testCreateCredentialsWithNullName() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(cvAPI.createUpdateCredentials(null)).
	        log().all().
	    when().
	        post(getHost() + uri + "credentials").
	    then().
	    	log().all().
	        statusCode(400);
	}


	
}
