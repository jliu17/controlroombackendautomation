package com.AA.credentialVaultTests.sanity;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.Test;

import com.AA.CommonUtil.BaseTest;
import com.AA.credentialVaultAPI.CredentialVaultAPI;

public class KeysTest extends BaseTest {
	String uri = "/v1/credentialvault/";
	String keyValue = "atest";
	int id = 1;
	CredentialVaultAPI cvAPI = new CredentialVaultAPI();
	
	
	@Test//(enabled=false)
	public void testCreateKeys() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(cvAPI.createUpdateKeys("asfasfd")).
	        log().all().
	    when().
	        post(getHost() + uri + "keys").
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

	@Test(enabled=false)
	public void testUpdateKeys() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(cvAPI.createUpdateKeys(keyValue + "Updated")).
	        log().all().
	    when().
	        put(getHost() + uri + "keys").
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

	@Test//(enabled=false)
	public void testGetKeys() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "keys").
	    then().
	    	log().all().
	        statusCode(200);
	}
	
}
