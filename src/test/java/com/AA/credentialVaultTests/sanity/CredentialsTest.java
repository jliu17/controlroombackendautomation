package com.AA.credentialVaultTests.sanity;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.AA.CommonUtil.BaseTest;
import com.AA.credentialVaultAPI.CredentialVaultAPI;

import io.restassured.response.Response;

public class CredentialsTest extends BaseTest {
	String uri = "/v1/credentialvault/";
	String name = "automationtest";
	int id;
	Response response;
	CredentialVaultAPI cvAPI = new CredentialVaultAPI();
	
	
	@Test
	public void testCreateCredentials() throws Exception {
		response = 
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(cvAPI.createUpdateCredentials(name)).
	        log().all().
	    when().
	        post(getHost() + uri + "credentials").
	    then().
	    	log().all().
	        body(containsString(name)).
	        statusCode(201).
	    extract().
	    	response();
		id = response.path("id");
		assertTrue(id > 0);
	}

	@Test(dependsOnMethods = { "testCreateCredentials" })
	public void testUpdateCredentials() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(response.asString().replace(name, name + "Updated")).
	        log().all().
	    when().
	        put(getHost() + uri + "credentials/" + id).
	    then().
	    	log().all().
	        statusCode(200);
	}

	@Test(dependsOnMethods = { "testUpdateCredentials" })
	public void testGetCredentials() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "credentials").
	    then().
	    	log().all().
	        body(containsString(name)).
	        statusCode(200);
	}
	
	@Test(dependsOnMethods = { "testGetCredentials" })
	public void testDeleteCredentials() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        delete(getHost() + uri + "credentials/" + id).
	    then().
	    	log().all().
	        statusCode(204);
	}
	
	@Test(dependsOnMethods = { "testDeleteCredentials" })
	public void testGetCredentialByIdAfterDelete() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "credentials/" + id).
	    then().
	    	log().all().
	        statusCode(404);
	}

}
