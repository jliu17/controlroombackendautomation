package com.AA.credentialVaultTests.sanity;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.AA.CommonUtil.BaseTest;
import com.AA.credentialVaultAPI.CredentialVaultAPI;

import io.restassured.response.Response;

public class AttributeValuesTest extends BaseTest {
	String uri = "/v1/credentialvault/";
	String name = "automation" + random;
	int id;
	int attrId;
	String attributeValue = "atest";
	Response response;
	CredentialVaultAPI cvAPI = new CredentialVaultAPI();
	
	
	@BeforeClass
	public void testCreateCredentials() throws Exception {
		response = 
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(cvAPI.createUpdateCredentials(name)).
	        log().all().
	    when().
	        post(getHost() + uri + "credentials").
	    then().
	    	log().all().
	        body(containsString(name)).
	        statusCode(201).
	    extract().
	    	response();
		id = response.path("id");
		assertTrue(id > 0);
	}

	@Test(priority = 1)
	public void testCreateCredentialAttributeValues() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(cvAPI.createUpdateAttributeValues(attributeValue, attrId)).
	        log().all().
	    when().
	        post(getHost() + uri + "credentials/" + id + "/attributevalues").
	    then().
	    	log().all().
	        statusCode(200);
	}

	@Test(priority = 2)
	public void testGetCredentialAttributeValues() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "credentials/" + id + "/attributevalues").
	    then().
	    	log().all().
	        body(containsString(name)).
	        statusCode(200);
	}
	
	@AfterClass
	public void testDeleteCredentials() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        delete(getHost() + uri + "credentials/" + id).
	    then().
	    	log().all().
	        statusCode(204);
	}
	

}
