package com.AA.userManagementTests.sanity;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.Test;

import com.AA.CommonUtil.BaseTest;
import com.AA.userManagementAPI.UserManagementAPI;

public class RoleTest extends BaseTest {
	String uri = "/v1/usermanagement/";
	String roleName = "testAdmin";
	String userName = "testAdmin";
	String pswd = "automationtester";
	String email = "testAdmin@automation.com";
	int id = 0;
	UserManagementAPI umAPI = new UserManagementAPI();
	
	@Test//(enabled=false)
	public void testCreateRoles() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(umAPI.createUpdateRolesPayload(roleName, userName)).
	        log().all().
	    when().
	        post(getHost() + uri + "roles").
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

	@Test(enabled=false)
	public void testUpdateRoles() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(umAPI.createUpdateRolesPayload(roleName+"Updated", userName+"Updated")).
	        log().all().
	    when().
	        put(getHost() + uri + "roles/" + id).
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

	@Test(enabled=false)
	public void testGetRoles() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "roles").
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}
	
	@Test(enabled=false)
	public void testAssignPrincipals() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(umAPI.assignPrincipals(0)).
	        log().all().
	    when().
	        post(getHost() + uri + "roles/" + id + "/principals").
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

}
