package com.AA.userManagementTests.sanity;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.Test;

import com.AA.CommonUtil.BaseTest;
import com.AA.userManagementAPI.UserManagementAPI;

public class UserTest extends BaseTest {
	String uri = "/v1/usermanagement/";
	String roleName = "testAdmin";
	String userName = "testAdmin";
	String pswd = "automationtester";
	String email = "testAdmin@automation.com";
	int id = 1;
	UserManagementAPI umAPI = new UserManagementAPI();
	
	
	@Test(enabled=false)
	public void testCreateUsers() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(umAPI.createUpdateUsersPayload(email, userName, pswd)).
	        log().all().
	    when().
	        post(getHost() + uri + "users").
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

	@Test//(enabled=false)
	public void testUpdateUserById() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        body(umAPI.createUpdateUsersPayload(email, userName, pswd)).
	        log().all().
	    when().
	        put(getHost() + uri + "users/" + null).
	    then().
	    	log().all().
	        body(containsString("id")).
	        statusCode(200);
	}

	@Test(enabled=false)
	public void testGetUsers() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "users").
	    then().
	    	log().all().
	        body(containsString("username")).
	        statusCode(200);
	}
	
	@Test(enabled=false)
	public void testGetUserById() throws Exception {
		given().
			header("Authorization", "Bearer " + token).
	        contentType("application/json").
	        log().all().
	    when().
	        get(getHost() + uri + "users/" + id).
	    then().
	    	log().all().
	        body(containsString("username")).
	        statusCode(200);
	}


}
