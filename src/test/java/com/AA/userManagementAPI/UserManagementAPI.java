package com.AA.userManagementAPI;

public class UserManagementAPI {


	public String createUpdateRolesPayload(String roleName, String userName) {
		String body = "{\r\n" + 
				"  \"id\": null,\r\n" + 
				"  \"name\": \"%s\",\r\n" + 
				"  \"description\": \"automation test\",\r\n" + 
				"  \"permissions\": [\r\n" + 
				"    {\r\n" + 
				"      \"id\": 999,\r\n" + 
				"      \"action\": \"test\",\r\n" + 
				"      \"resourceId\": \"test\",\r\n" + 
				"      \"resourceType\": \"test\"\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"principals\": [\r\n" + 
				"    {\r\n" + 
				"      \"id\": 0,\r\n" + 
				"      \"username\": \"%s\",\r\n" + 
				"      \"subjectId\": \"9\",\r\n" + 
				"      \"domain\": \"null\",\r\n" + 
				"      \"autoLoginEnabled\": true,\r\n" + 
				"      \"deleted\": false,\r\n" + 
				"      \"emailVerified\": true,\r\n" + 
				"      \"pwdExpired\": false\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		return String.format(body, roleName, userName);
	}

	public String createUpdateUsersPayload(String email, String userName, String password) {
		String body = "{\r\n" + 
				"  \"roles\": [\r\n" + 
				"    {\r\n" + 
				"      \"id\": 1\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"domain\": null,\r\n" + 
				"  \"email\": \"%s\",\r\n" + 
				"  \"enableAutoLogin\": true,\r\n" + 
				"  \"clientRegistered\": false,\r\n" + 
				"  \"username\": \"%s\",\r\n" + 
				"  \"firstName\": \"automation\",\r\n" + 
				"  \"lastName\": \"tester\",\r\n" + 
				"  \"disabled\": false,\r\n" + 
				"  \"password\": \"%s\"\r\n" + 
				"}";
		return String.format(body, email, userName, password);
	}
	
	public String assignPrincipals(int id) {
		String body = "[\r\n" + 
				"  {\r\n" + 
				"    \"id\": %d\r\n" + 
				"  }\r\n" + 
				"]";
		return String.format(body, id);
	}

}
