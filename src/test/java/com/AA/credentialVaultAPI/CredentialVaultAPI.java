package com.AA.credentialVaultAPI;

public class CredentialVaultAPI {


	public String createUpdateKeys(String keyValue) {
		String body = "{\r\n" + 
				"  \"name\": \"PrivateKey\",\r\n" + 
				"  \"value\": \"%s\"\r\n" + 
				"}";
		return String.format(body, keyValue);
	}
	
	public String createUpdateCredentials(String name) {
		String body = "{\r\n" + 
				"  \"name\": \"%s\",\r\n" + 
				"  \"description\": \"string\",\r\n" + 
				"  \"attributes\": [\r\n" + 
				"    {\r\n" + 
				"      \"name\": \"%s\",\r\n" + 
				"      \"description\": \"test\",\r\n" + 
				"      \"userSpecific\": true,\r\n" + 
				"      \"masked\": true\r\n" + 
				"    }\r\n" + 
				"  ]\r\n" + 
				"}";
		return String.format(body, name, name);
	}

	public String createUpdateAttributeValues(String attributeValue, int attributeId) {
		String body = "[\r\n" + 
				"  {\r\n" + 
				"    \"credentialAttributeId\": %d,\r\n" + 
				"    \"value\": \"%s\"\r\n" + 
				"  }\r\n" + 
				"]";
		return String.format(body, attributeId, attributeValue);
	}
	
}
