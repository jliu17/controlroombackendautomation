package com.AA.CommonUtil;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.BeforeClass;

import io.restassured.response.Response;


public class BaseTest {
	protected static Properties properties = null;
	protected static InputStream inputstream = null;
	protected String token = null;
	protected String random = RandomStringUtils.randomAlphabetic(10);
	
	static {
		try {
			properties = new Properties();
			inputstream = BaseTest.class.getClassLoader().getResourceAsStream("datafile.properties");
			properties.load(inputstream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getHost() {
		String url = properties.getProperty("host");
		return url;
	}

	public static String[] getUserNamePassword() {
		return  new String[] {(String) properties.get("usrName"), (String) properties.get("usrPassword")};
	}

	@BeforeClass(groups = {"smoke"})
	public void testSetUp() throws Throwable {
		String body = "{\"username\": \"%s\", \"password\": \"%s\"}";
		body = String.format(body, getUserNamePassword()[0], getUserNamePassword()[1]);
	Response response = 
	given().
        contentType("application/json").
    	body(body).
    	log().all().
	when().
        post(getHost() + "/v1/authentication").
    then().
    	log().all().
        statusCode(200).
	extract().
    	response();
	token = response.path("token");
	assertTrue(token.length() > 200);
	}


}
